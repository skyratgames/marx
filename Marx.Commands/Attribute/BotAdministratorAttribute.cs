﻿using Discord.Commands;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Marx.Commands.Attribute
{
	public sealed class BotAdministratorAttribute : PreconditionAttribute
	{
		public override Task<PreconditionResult> CheckPermissionsAsync(ICommandContext context, CommandInfo command, IServiceProvider services)
		{
			var config = services.GetService<Marx.Utility.MarxConfig>();

			var admins = config.Bot.Administrators;

			if (admins.Contains(context.User.Id))
				return Task.FromResult(PreconditionResult.FromSuccess());
			
			return Task.FromResult(PreconditionResult.FromError($"{context.User.Username}#{context.User.Discriminator} is not in the sudoers file. This incident will be reported."));

		}
	}
}
