﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Marx.Database;
using Marx.Cache;
using Marx.Utility.Pagination;
using Marx.Database.Model;

namespace Marx.Handlers
{
	public class DiscordEventHandler
	{

		private DiscordSocketClient _client;
		private DbContext _dbContext;
		private RedisCacheHandler _redis;

		private Dictionary<ulong, PaginatedEmbed> paginatedMessages = new Dictionary<ulong, PaginatedEmbed>();

		public DiscordEventHandler(DiscordSocketClient client, DbContext dbContext, RedisCacheHandler redis)
		{
			this._client = client;
			this._dbContext = dbContext;
			this._redis = redis;
		}

		/// EVENTS
		public Task MessageDeleted(Cacheable<IMessage, ulong> message, ISocketMessageChannel channel)
		{
			// TODO: Remove emojiroles from guild for all that were attached to this message.
			return Task.CompletedTask;
		}

		public async Task Ready()
		{
			await _client.SetGameAsync("The Communist Manifesto by Karl Marx & Friedrich Engels", type: ActivityType.Listening);
			await _client.SetStatusAsync(UserStatus.Online);

		}

		public Task Disconnected(Exception arg)
		{
			return Task.CompletedTask;
		}

		public async Task ReactionAdded(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
		{

			if (reaction.UserId == _client.CurrentUser.Id)
				return;

			paginatedMessages.TryGetValue(message.Id, out PaginatedEmbed paginator);

			if (paginator != null && paginator.IsActive)
				await paginator.ReactionAdded(message, channel, reaction);
		}

		/// EVENT RELATED STUFF
		public void AddPaginationListener(PaginatedEmbed pageinator)
		{
			if(pageinator.IsActive)
				paginatedMessages.Add(pageinator.MessageId, pageinator);
		}
	}
}
