using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Marx.Commands.Parsing
{
    public class TimeSpanParser
    {
        private static readonly Dictionary<string, int> units = new Dictionary<string, int>()
        {
            {@"(\d+)(ms|millisecond[|s])", 1},
            {@"(\d+)(s|sec|second[|s])", 1000},
            {@"(\d+)(m|min|minute[|s])", 60000},
            {@"(\d+)(h|hour[|s])", 3600000},
            {@"(\d+)(d|day[|s])", 86400000},
            {@"(\d+)(w|week[|s])", 604800000}
        };

        public static TimeSpan Parse(string input)
        {
            var timespan = new TimeSpan();

            foreach (var x in units)
            {
                var matches = Regex.Matches(input, x.Key);
                foreach (Match match in matches)
                {
                    var amount = System.Convert.ToInt32(match.Groups[1].Value);
                    timespan = timespan.Add(TimeSpan.FromMilliseconds(x.Value * amount));
                }
            }

            return timespan;
        }
    }
}
