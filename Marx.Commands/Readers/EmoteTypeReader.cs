using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace Marx.Commands
{
    public class EmoteTypeReader<T> : TypeReader where T : class, IEmote
    {
        public override async Task<TypeReaderResult> ReadAsync(ICommandContext context, string input, System.IServiceProvider services)
        {
            Emote emote = null;
            Emote.TryParse(input, out emote);

            if (emote != null)
            {
                return TypeReaderResult.FromSuccess(emote);
            }
            else
            {
                if (NeoSmart.Unicode.Emoji.IsEmoji(input))
                {
                    return TypeReaderResult.FromSuccess(new Discord.Emoji(input));
                }
                return TypeReaderResult.FromError(CommandError.ObjectNotFound, "Not a valid emote!");
            }
        }
    }
}
