﻿using Discord;
using Discord.Commands;
using Marx.Cache;
using Marx.Database;
using Marx.Database.Model;
using Marx.Handlers;
using Marx.Utility;
using Marx.Utility.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Marx.Commands.Modules
{
	public class UtilityModule : ModuleBase<SocketCommandContext>
	{

		public MarxConfig _config { get; set; }
		public DbContext _dbContext { get; set; }
		public RedisCacheHandler _cache { get; set; }
		public CommandService _cmdService { get; set; }
		public IServiceProvider _services { get; set; }
		public DiscordEventHandler _events { get; set; }

		private Thread helpThread;

		[Command("quickpoll")]
		[Alias("qp", "qpoll")]
		[Summary("Creates a quick poll.")]
		[RequireBotPermission(ChannelPermission.AddReactions | ChannelPermission.SendMessages | ChannelPermission.EmbedLinks)]
		public async Task QPAsync([Remainder] [Summary("Question to ask")] string pollq)
		{
			var cachedGuild = _cache.Get<DBGuild>(Context.Guild.Id.ToString());

			var embed = new EmbedBuilder
			{
				Title = "Quick Poll",
				Description = pollq
			};

			IUserMessage msg = await ReplyAsync(embed: embed.Build());

			DBQuickPollSettings qp = cachedGuild.Settings.QuickPollSettings;
			IEmote[] emotes = new IEmote[] { qp.AffirmativeEmote, qp.NegativeEmote };

			await msg.AddReactionsAsync(emotes);
		}

		[Command("poll")]
		[Summary("Creates a poll.")]
		[RequireBotPermission(ChannelPermission.AddReactions | ChannelPermission.SendMessages)]
		public async Task PollAsync(string question, params string[] options)
		{
			if (options.Length > 20 || options.Length < 2)
			{
				await ReplyAsync("You must have between 2 and 20 options!");
			}
			else
			{
				StringBuilder sb = new StringBuilder();

				for (int i = 0; i < options.Length; i++)
				{
					sb.Append($"{new RegionalIndicator(i)} {options[i]}");

					if (i != options.Length - 1)
					{
						sb.Append("\n");
					}
				}

				EmbedBuilder embed = new EmbedBuilder()
				{
					Title = "Poll",
					Description = question
				};
				embed.AddField("Options", sb.ToString());

				IUserMessage msg = await ReplyAsync(embed: embed.Build());

				for (int i = 0; i < options.Length; i++)
				{
					await msg.AddReactionAsync(new Emoji(new RegionalIndicator(i).ToString()));
				}
			}
		}

		[Command("help")]
		[Summary("Shows this help menu.")]
		[RequireBotPermission(ChannelPermission.AddReactions | ChannelPermission.SendMessages | ChannelPermission.EmbedLinks)]
		public void HelpAsync()
		{
			helpThread = new Thread(HelpThread);
			helpThread.Start();
		}

		private void HelpThread()
		{

			int PAGE_LENGTH = 10;

			
			List<CommandInfo> commands = _cmdService.GetExecutableCommandsAsync(Context, _services).GetAwaiter().GetResult().Distinct(new CommandInfoComparer()).ToList();

			var total = (commands.Count / PAGE_LENGTH);

			EmbedPaginationBuilder pageBuilder = new EmbedPaginationBuilder(userId: Context.User.Id);

			for (int i = 0; i <= total; i++)
			{
				var cmds = commands.Skip(i * PAGE_LENGTH).Take(PAGE_LENGTH).ToList();
				if(cmds.Count >= 1)
					pageBuilder.AddPage(GetHelpPage(cmds));
			}

			var pager = pageBuilder.Build(Context.Channel, Context.Client);

			_events.AddPaginationListener(pager);
		}

		private Embed GetHelpPage(List<CommandInfo> cmds)
		{
			EmbedBuilder eb = new EmbedBuilder();
			cmds.ForEach(cmd =>
			{
				var fb = new EmbedFieldBuilder();
				fb.Name = cmd.Name;
				fb.Value = cmd.Summary != null ? cmd.Summary : "no description provided. FIX IT";
				eb.AddField(fb);
			});
			return eb.Build();

		}
    }
}
