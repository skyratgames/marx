﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Marx.Commands.Parsing;
using System;
using Marx.Cache;
using Marx.Database;
using Marx.Database.Model;
using MongoDB.Bson;

namespace Marx.Commands.Modules
{
    public class ModerationModule : ModuleBase<SocketCommandContext>
    {
        public DbContext context { get; set; }
        public RedisCacheHandler redis { get; set; }

        [Command("ban")]
        [Summary("Bans a user from the server.")]
        [RequireBotPermission(GuildPermission.BanMembers)]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public Task BanAsync(IGuildUser userToBan, [Remainder] string reason) => BanAsync(userToBan, 0, reason);

        [Command("ban")]
        [Summary("Bans a user from the server.")]
        [RequireBotPermission(GuildPermission.BanMembers)]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public async Task BanAsync(IGuildUser userToBan, int prune, [Remainder] string reason)
        {
            await userToBan.SendMessageAsync($"You have been banned from {Context.Guild.Name} for: {reason}");
            await userToBan.BanAsync(pruneDays: prune, reason: $"{Context.Message.Author.Username}#{Context.Message.Author.Discriminator}: {reason}");

			var guild = await context.GetGuildAsync(Context.Guild.Id);

			DBDisciplinaryAction action = new DBDisciplinaryAction();
			action.Id = ObjectId.GenerateNewId();
			action.UserId = userToBan.Id;
			action.Reason = reason;
			action.Type = DBDisciplinaryActionType.Ban;
			action.Expires = DateTime.MaxValue;
			// its forever fuck u.
			action.ExpirationProcessed = true;

            await ReplyAsync($"{userToBan.Mention} has been banned.");
        }
		[Command("tempban")]
		[Alias("tban")]
		[Summary("Temporarily bans a user from the server.")]
		[RequireBotPermission(GuildPermission.BanMembers)]
		[RequireUserPermission(GuildPermission.BanMembers)]
		public async Task TempBanAsync(IGuildUser userToBan, string length, [Remainder] string reason) => await TempBanAsync(userToBan, length, 0, reason);

		[Command("tempban")]
        [Alias("tban")]
        [Summary("Temporarily bans a user from the server.")]
        [RequireBotPermission(GuildPermission.BanMembers)]
        [RequireUserPermission(GuildPermission.BanMembers)]
        public async Task TempBanAsync(IGuildUser userToBan, string length, int prune, [Remainder] string reason)
        {
            TimeSpan unbanTime = TimeSpanParser.Parse(length);
            var guild = await context.GetGuildAsync(Context.Guild.Id);

            DBDisciplinaryAction action = new DBDisciplinaryAction();
			action.Id = ObjectId.GenerateNewId();
            action.UserId = userToBan.Id;
            action.Reason = reason;
            action.Type = DBDisciplinaryActionType.Ban;
            action.Expires = DateTime.UtcNow + unbanTime;
            action.ExpirationProcessed = false;

            var result = await guild.AddDisciplinaryActionAsync(action, context);
            if (!result.Success)
            {
                await ReplyAsync(result.Reason);
            }
            else
            {
                await userToBan.SendMessageAsync($"You have been banned from {Context.Guild.Name} for {reason}. Your ban will last {unbanTime.ToString()}");
                await userToBan.BanAsync(pruneDays: prune, reason: $"{Context.Message.Author.Username}#{Context.Message.Author.Discriminator} {reason}");
                await ReplyAsync($"{userToBan.Mention} has been banned.");
            }
        }

		[Command("mute")]
		[Summary("Mutes the user for a specified time or forever")]
		[RequireBotPermission(GuildPermission.ManageRoles)]
		[RequireUserPermission(GuildPermission.ManageRoles)]
		public async Task MuteUserAsync(IGuildUser user, string time, [Remainder] string reason)
		{
			TimeSpan unmuteTime = TimeSpanParser.Parse(time);
			DateTime unmuteTimeDT = DateTime.UtcNow + unmuteTime;
			var guild = await context.GetGuildAsync(Context.Guild.Id);

			if(guild.Settings.MuteRoleId == 0)
			{
				await ReplyAsync($"You have not defined a mute role. Use `{guild.Settings.Prefix}settings muteRole @<Existing Role Name or Mention>` or `{guild.Settings.Prefix}settings createMuteRole <Optional Role Name>`");
				return;
			}

			DBDisciplinaryAction action = new DBDisciplinaryAction();
			action.Id = ObjectId.GenerateNewId();
			action.UserId = user.Id;
			action.Reason = reason;
			action.Type = DBDisciplinaryActionType.Mute;
			action.Expires = unmuteTimeDT;
			action.ExpirationProcessed = false;

			var result = await guild.AddDisciplinaryActionAsync(action, context);
			if (!result.Success)
			{
				await ReplyAsync(result.Reason);
			}
			else
			{
				await user.AddRoleAsync(Context.Guild.GetRole(guild.Settings.MuteRoleId));
				await ReplyAsync($"{user.Mention} has been muted until {unmuteTimeDT.ToString("yyyy-MM-dd hh:mm:ss K")}");
			}
		}

		[Command("kick")]
        [Summary("Kicks a user from the server.")]
        [RequireBotPermission(GuildPermission.KickMembers)]
        [RequireUserPermission(GuildPermission.KickMembers)]
        public async Task KickAsync(IGuildUser userToKick, [Remainder] string reason)
        {
            await userToKick.SendMessageAsync($"You have been kicked from {Context.Guild.Name} for: {reason}");
            Task kick = userToKick.KickAsync($"{Context.Message.Author.Username}#{Context.Message.Author.Discriminator}: {reason}");
            kick.Wait();

            if (kick.IsCompletedSuccessfully)
            {
				var guild = await context.GetGuildAsync(Context.Guild.Id);

				DBDisciplinaryAction action = new DBDisciplinaryAction();
				action.Id = ObjectId.GenerateNewId();
				action.UserId = userToKick.Id;
				action.Reason = reason;
				action.Type = DBDisciplinaryActionType.Kick;
				action.Expires = DateTime.UtcNow;
				// its instant so I mean its already processed.
				action.ExpirationProcessed = true;

				await ReplyAsync($"{userToKick.Mention} has been kicked.");
            }
        }

		[Command("purge")]
        [Alias("remove")]
        [Summary("Cleans messages in a channel")]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task PurgeAsync(int msgNumber)
        {
            await Context.Message.DeleteAsync();

            List<IMessage> messages = (await Context.Channel.GetMessagesAsync(msgNumber).FlattenAsync()).ToList<IMessage>();

            await (Context.Channel as SocketTextChannel).DeleteMessagesAsync(messages);
        }
    }
}
