﻿using Discord;
using Discord.Commands;
using Discord.Rest;
using Marx.Cache;
using Marx.Database;
using Marx.Database.Model;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Marx.Commands.Modules
{
	[Group("settings")]
	public class SettingsModule : ModuleBase<SocketCommandContext>
	{

		public DbContext dbContext { get; set; }
		public RedisCacheHandler cache { get; set; }

		[Command("mute")]
		public async Task SetMuteRoleAsync(IRole role)
		{
			DBGuild guild = await dbContext.GetGuildAsync(Context.Guild.Id);
			guild.Settings.MuteRoleId = role.Id;

			await guild.Settings.UpdateAsync(guild.Id, dbContext, cache);

			await ReplyAsync($"Role {role.Name} assigned for muting.");
		}

		[Command("createMuteRole")]
		public async Task CreateMuteRoleAsync(string roleName = "Mute")
		{
			if(roleName.Length > 100)
			{
				await ReplyAsync("You can't have a role name longer than 255 characters. Please shorten your name.");
				return;
			}

			DBGuild guild = await dbContext.GetGuildAsync(Context.Guild.Id);

			RestRole role = await Context.Guild.CreateRoleAsync(roleName, GuildPermissions.None);

			guild.Settings.MuteRoleId = role.Id;

			await guild.Settings.UpdateAsync(guild.Id, dbContext, cache);

			await ReplyAsync($"Role {roleName} created and assigned to for muting.");
		}

		[Command("quickpoll")]
		[Alias("qp")]
		[Summary("Sets quick poll affirmative emoji.")]
		public async Task QPEmojiAsync(string type, IEmote emote)
		{

			// TODO: Implement new database stuff when thats introduced.
			DBGuild guild = await dbContext.GetGuildAsync(Context.Guild.Id);
			DBSettings settings = guild.Settings;

			if (guild == null)
			{
				await ReplyAsync($"Things went bad, Please try again later. {NeoSmart.Unicode.Emoji.FrowningFace}");
				return;
			}

			switch (type)
			{
				case "affirm":
				case "affirmative":
					settings.QuickPollSettings.AffirmativeEmote = emote;
					break;

				case "neg":
				case "negative":
					settings.QuickPollSettings.NegativeEmote = emote;
					break;
				default:
					await ReplyAsync("QP emoji type invalid, Please use affirmative or negative. (affirm/neg)");
					break;
			}


			await settings.UpdateAsync(guild.Id, dbContext, cache);
		}

		[Command("prefix")]
		[Summary("Sets the bots prefix for this guild.")]
		public async Task PrefixAsync(string prefix)
		{
			DBGuild guild = await dbContext.GetGuildAsync(Context.Guild.Id);

			DBSettings settings = guild.Settings;

			if (guild == null)
			{
				await ReplyAsync($"Things went bad, Please try again later. {NeoSmart.Unicode.Emoji.FrowningFace}");
				return;
			}

			settings.Prefix = prefix;

			await settings.UpdateAsync(guild.Id, dbContext, cache);
		}
	}
}
