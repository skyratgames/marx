﻿using Discord.Commands;
using Marx.Cache;
using Marx.Database;
using Marx.Database.Model;
using System.Threading.Tasks;

namespace Marx.Commands.Modules
{
    public class TagsModule : ModuleBase<SocketCommandContext>
    {

        public DbContext context { get; set; }
        public RedisCacheHandler redis { get; set; }

        [Command("tag")]
        public async Task TagAsync()
        {
            await ReplyAsync("Must specify tag or command (add/del)");
        }

        [Command("tag")]
		[Alias("t")]
		public async Task TagAsync(string name)
        {
            var guild = await context.GetGuildAsync(Context.Guild.Id);
            var tag = guild.Tags.Find(t => t.Name == name || t.Aliases.Contains(name));
            if (!string.IsNullOrEmpty(tag.Content))
                await ReplyAsync($"{tag.Content}");
            else
                await ReplyAsync("Tag not found.");
        }

        [Command("tag")]
		public async Task TagAsync(string cmd, string arg1)
        {
            var guild = await context.GetGuildAsync(Context.Guild.Id);
            DatabaseResult res = null;

            switch (cmd.ToLower())
            {
                case "remove":
                case "delete":
                    res = await guild.RemoveTagAsync(arg1, context, redis);
                    if (!res.Success)
                        await ReplyAsync(res.Reason);
                    else
                        await ReplyAsync($"Tag {arg1} deleted");
                    break;

                default:
                    await ReplyAsync("Tag subcommand not found.");
                    break;
            }
        }

        [Command("tag")]
		public async Task TagAsync(string cmd, string arg1, [Remainder]string remaining)
        {

            var guild = await context.GetGuildAsync(Context.Guild.Id);
            DatabaseResult res = null;

            switch (cmd.ToLower())
            {

                case "add":
                    DBTag addTag = new DBTag(arg1, remaining);
                    res = await guild.AddTagAsync(addTag, context, redis);
                    if (!res.Success)
                        await ReplyAsync(res.Reason);
                    else
                        await ReplyAsync($"Tag {arg1} Created");
                    break;

                case "addalias":
                case "aliasadd":
                    if (remaining.Contains(" "))
                    {
                        await ReplyAsync("Alias cannot contain a space!");
                        break;
                    }

                    res = await guild.AddTagAliasAsync(arg1, remaining, context, redis);
                    if (!res.Success)
                        await ReplyAsync(res.Reason);
                    else
                        await ReplyAsync($"Tag alias {remaining} added to {arg1}");
                    break;

                case "removealias":
                case "aliasremove":
                case "deletealias":
                case "aliasdelete":
                    if (remaining.Contains(" "))
                    {
                        await ReplyAsync("Alias cannot contain a space!");
                        break;
                    }

                    res = await guild.RemoveTagAliasAsync(arg1, remaining, context, redis);
                    if (!res.Success)
                        await ReplyAsync(res.Reason);
                    else
                        await ReplyAsync($"Tag alias {remaining} removed from {arg1}");
                    break;

                default:
                    await ReplyAsync("Tag subcommand not found.");
                    break;
            }
        }
    }
}
