﻿using Discord.Commands;
using Marx.Commands.Attribute;
using Marx.Database;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Marx.Commands.Modules
{
	[BotAdministrator]
    public class BotAdminModule : ModuleBase<SocketCommandContext>
    {
        public DbContext dbContext { get; set; }

        [Command("say")]
        [Summary("Echoes a message.")]
        public Task SayAsync([Remainder] [Summary("Text to echo")] string echo)
        {
            Context.Message.DeleteAsync();
            return ReplyAsync(echo);
        }

        [Command("saytts")]
		[Summary("Echoes a message but with TTS.")]
        public Task SayTTSAsync([Remainder] string echo)
        {
            Context.Message.DeleteAsync();
            return ReplyAsync(isTTS: true, message: echo);
        }


    }
}
