﻿using System;
using System.Threading.Tasks;
using Marx.Handlers;
using Marx.Database;
using Marx.Utility;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Marx.Cache;
using System.Threading;
using Marx.Database.Model;
using System.Collections.Generic;
using MongoDB.Driver;
using System.Linq;

namespace Marx
{
    class Program
    {
        private readonly DiscordSocketClient _client;
        public readonly MarxConfig _config;
        private readonly CommandService _commandService;
        private readonly CommandHandler _commandHandler;
        private readonly DbContext _dbContext;
		private readonly RedisCacheHandler _redis;
		private readonly DiscordEventHandler _events;
		private readonly Thread _DisciplinaryActionWorkerThread;

		private bool DoActionCheck = true;


        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private Program()
        {

			// Initialize Configuration and bind it otherwise exit.
            try
            {
				var Iconfig = new ConfigurationBuilder()
					   .AddYamlFile("configuration.yaml", false, true)
					   .Build();

				_config = new MarxConfig();

				ConfigurationBinder.Bind(Iconfig, _config);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Environment.Exit(-1);
            }

			// Create and initialize all services and clients.

			CommandServiceConfig cmdConfig = new CommandServiceConfig();
			DiscordSocketConfig clientConfig = new DiscordSocketConfig();

			clientConfig.MessageCacheSize = 50;
			clientConfig.ExclusiveBulkDelete = false;

			_client = new DiscordSocketClient(clientConfig);
			
			cmdConfig.DefaultRunMode = RunMode.Async;


			_commandService = new CommandService(cmdConfig);

            _dbContext = new DbContext(_config.Bot.MongoString, _config.Bot.DbName);

			_redis = new RedisCacheHandler(_config.Bot.RedisString);

			_events = new DiscordEventHandler(_client, _dbContext, _redis);

			_commandHandler = new CommandHandler(_client, _commandService, _config, _dbContext, _redis, _events);

			_DisciplinaryActionWorkerThread = new Thread(DisciplinaryActionWorker);

        }

        public async Task MainAsync()
        {

            _client.Log += Log;

			AddEvents();

            await _client.LoginAsync(TokenType.Bot, _config.Bot.Token);

            await _client.StartAsync();

			await _commandHandler.InstallTypeReadersAsync();

            await _commandHandler.InstallCommandsAsync();

			_client.Ready += () =>
			{
				_DisciplinaryActionWorkerThread.Start();
				return Task.CompletedTask;
			};

			await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

		private void AddEvents()
		{
			_client.Ready += _events.Ready;
			_client.Disconnected += _events.Disconnected;
			_client.MessageDeleted += _events.MessageDeleted;
			_client.ReactionAdded += _events.ReactionAdded;
		}

		private void DisciplinaryActionWorker()
		{
			while(DoActionCheck)
			{
				var guilds = _dbContext.GetDatabase().GetCollection<DBGuild>("guilds").Find(g => true).ToList();

				RequestOptions daExpire = RequestOptions.Default;
				daExpire.AuditLogReason = "Marx Disciplinary Action Worker - Action Expired.";

				guilds.ForEach(guild =>
				{

					List<DBDisciplinaryAction> readyToProccess = guild.DisciplinaryActions.FindAll(da => !da.ExpirationProcessed && da.Expires <= DateTime.UtcNow);

					var dGuild = _client.Guilds.FirstOrDefault(g => g.Id == guild.Id);

					if (dGuild == null)
						return;

					var MuteRole = dGuild.GetRole(guild.Settings.MuteRoleId);

					readyToProccess.ForEach(da =>
					{
						Console.WriteLine($"DEBUG: Processing {da.Type} from {guild.Id} for {da.UserId}.");
						switch (da.Type)
						{
							case DBDisciplinaryActionType.Ban:
								dGuild.RemoveBanAsync(da.UserId, daExpire);
								break;
							case DBDisciplinaryActionType.Mute:
								if (MuteRole == null)
									break;
									dGuild.GetUser(da.UserId)?.RemoveRoleAsync(MuteRole, daExpire);
								break;
							default:
								Console.Error.WriteLine($"Disciplinary Action #{da.Id} of type {da.Type} was marked for processing but isn't processable. Skipping.");
								return;
						}

						da.ExpirationProcessed = true;
						da.Update(guild.Id, _dbContext);
					});
				});

				Thread.Sleep(_config.Bot.DisciplinaryActionTimer);
			}
		}

	}
}
