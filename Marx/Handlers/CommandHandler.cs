using System.Threading.Tasks;
using Discord.WebSocket;
using Discord.Commands;
using Marx.Cache;
using Marx.Database;
using Marx.Utility;
using System;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Marx.Database.Model;
using Discord;
using Marx.Commands;

namespace Marx.Handlers
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly MarxConfig _config;
        private readonly ServiceProvider _services;
		
        private readonly DbContext _dbContext;
		private readonly RedisCacheHandler _redis;
		private readonly DiscordEventHandler _events;

        public CommandHandler(DiscordSocketClient client, CommandService commands, MarxConfig config, DbContext dbContext, RedisCacheHandler redis, DiscordEventHandler events)
        {
            _config = config;
            _commands = commands;
            _client = client;
            _dbContext = dbContext;
			_redis = redis;
			_events = events;
			_services = new ServiceCollection()
				.AddSingleton(_client)
				.AddSingleton(_commands)
				.AddSingleton(_config)
				.AddSingleton(_dbContext)
				.AddSingleton(_redis)
				.AddSingleton(_events)
                .BuildServiceProvider();
        }

        public async Task InstallCommandsAsync()
        {
            // Hook MessageReceived into the command handler.
            _client.MessageReceived += HandleCommandAsync;


            //TODO: Automate loading of assemblies.
            await _commands.AddModulesAsync(assembly: typeof(Marx.Commands.Modules.BotAdminModule).Assembly,
                                            services: _services);
        }

		public Task InstallTypeReadersAsync()
		{
			_commands.AddTypeReader<IEmote>(new EmoteTypeReader<IEmote>());

			return Task.CompletedTask;
		}

		private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Stop. ~~Hammer time~~ System message.
            SocketUserMessage message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Get channel and return if it isn't in a guild.
            SocketGuildChannel channel = message.Channel as SocketGuildChannel;
            if (channel == null) return;

            //Create a number to track where the prefix ends and the command begins.
            int argPos = 0;

            // Determine if the message is a command based on the prefix
            // Bots bad too!

            var prefix = GetPrefixFromCache(channel.Guild);
            if (!(message.HasStringPrefix(prefix, ref argPos)) || message.Author.IsBot) return;

            Console.WriteLine($"{message.Author.Username}#{message.Author.Discriminator}: {message.Content}");

            // Create websocket-based context based on the message.
            SocketCommandContext context = new SocketCommandContext(_client, message);

            IResult result = await _commands.ExecuteAsync(
                context,
                argPos,
                services: _services
            );
			// Don't enable this, Its considered "Bad for the end user" or some shit

            //if (!result.IsSuccess)
                //await context.Channel.SendMessageAsync(result.ErrorReason);

        }

		private string GetPrefixFromCache(SocketGuild guild)
		{
			ulong guildId = guild.Id;
			var cacheGuild = _redis.Get<DBGuild>(guild.Id.ToString());

			if (cacheGuild != null)
				return cacheGuild.Settings.Prefix;

			return GetPrefixFromDatabase(guild);
		}

        private string GetPrefixFromDatabase(SocketGuild guild)
        {
            ulong guildId = guild.Id;
            var db = _dbContext.GetDatabase();
            var dbguild = db.GetCollection<DBGuild>("guilds").Find(g => g.Id == guildId).FirstOrDefault();

            if (dbguild == null)
            {
                dbguild = new DBGuild();
                dbguild.Id = guildId;

                db.GetCollection<DBGuild>("guilds").InsertOne(dbguild);
            }

			Task.Run( () => _redis.StoreAsync(guild.Id.ToString(), dbguild));
            return dbguild.Settings.Prefix;
        }
    }
}
