﻿using System;
using System.Text;
using Discord;
using Discord.Commands;

namespace Marx.Utility
{
    public static class CustomExtensions
    {
        public static bool IsNotNull(this string str)
        {
            return str != null;
        }

        public static bool IsNull(this string str)
        {
            return str == null;
        }

        public static bool IsNotNull(this IGuildUser user)
        {
            return user != null;
        }

        public static bool IsNull(this IGuildUser user)
        {
            return user == null;
        }

        public static string GetUnicodeRepresentation(this string str)
        {
            char[] chars = str.ToCharArray();
            StringBuilder sb = new StringBuilder();

            foreach (char c in chars)
            {
                sb.Append("\\u");
                sb.Append(String.Format("{0:x4}", (int)c).ToUpper());
            }

            return sb.ToString();
        }

		public static bool Equals(this Embed embed1, Embed embed2)
		{
			return embed1.Type == embed2.Type && embed1.Title == embed2.Title && embed1.Url == embed2.Url && embed1.Description == embed2.Description;
		}

		public static string ToString(this CommandInfo cmd)
		{
			return cmd.Name;
		}
    }
}
