﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using NeoSmart.Unicode;
using UEmoji = NeoSmart.Unicode.Emoji;

namespace Marx.Utility.Pagination
{

	public class EmbedPaginationBuilder
	{

		private ulong _userId = 0;
		private List<Embed> _pages = new List<Embed>();

		public EmbedPaginationBuilder(List<Embed> pages = null, ulong userId = 0)
		{
			_pages = pages != null ? pages : new List<Embed>();
			_userId = userId;
		}

		/// <summary>
		/// Adds a page to the builder
		/// </summary>
		/// <param name="page">Embed to add</param>
		/// <returns>this, for chaining.</returns>
		public EmbedPaginationBuilder AddPage(Embed page)
		{
			_pages.Add(page);
			return this;
		}

		/// <summary>
		/// removes a page from the builder
		/// </summary>
		/// <param name="page">Embed to remove</param>
		/// <returns>this, for chaining.</returns>
		public EmbedPaginationBuilder RemovePage(Embed page)
		{
			_pages.Remove(page);
			return this;
		}

		/// <summary>
		/// removes a page from the builder
		/// </summary>
		/// <param name="pageNumber">Page number to remove</param>
		/// <returns>this, for chaining.</returns>
		public EmbedPaginationBuilder RemovePage(int pageNumber)
		{
			_pages.Remove(_pages[pageNumber]);
			return this;
		}


		/// <summary>
		/// Sets user to listen for.
		/// (Listens for everyone if not set)
		/// 
		/// <param name="user">User Object to grab ID from</param>
		/// <returns></returns>
		public EmbedPaginationBuilder SetUser(IUser user)
		{
			return SetUser(user.Id);
		}

		/// <summary>
		/// Sets user to listen for.
		/// (Listens for everyone if not set)
		/// </summary>
		/// <param name="user">ID of the user to listen for</param>
		/// <returns></returns>
		public EmbedPaginationBuilder SetUser(ulong user)
		{
			_userId = user;
			return this;
		}

		/// <summary>
		/// Build's the Paginator and sends the message and activates it.
		/// </summary>
		/// <param name="channel">Text channel to send the message to.</param>
		/// <param name="_client">Discord client to listen with.</param>
		/// <returns></returns>
		public PaginatedEmbed Build(ISocketMessageChannel channel, DiscordSocketClient _client)
		{
			return new PaginatedEmbed(channel, _userId, _pages, _client);
		}
	}
}
