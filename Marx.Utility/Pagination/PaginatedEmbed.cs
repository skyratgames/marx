﻿using Discord;
using Discord.WebSocket;
using NeoSmart.Unicode;
using System.Collections.Generic;
using System.Timers;
using System.Threading.Tasks;
using UEmoji = NeoSmart.Unicode.Emoji;
using DEmoji = Discord.Emoji;
using System.Linq;
using System;

namespace Marx.Utility.Pagination
{
	public class PaginatedEmbed
	{
		public const string
			   FIRST_PAGE = "\u23EE",
			   PREV_PAGE = "\u25C0",
			   STOP = "\u23F9",
			   NEXT_PAGE = "\u25B6",
			   LAST_PAGE = "\u23ED";

		private ulong _userId;
		private IUserMessage _message;
		private int _pageNum;
		private List<Embed> _pages;
		private bool _ended = false;

		private Timer _endgame;

		private DiscordSocketClient _client;


		public bool IsActive { get {
				return !_ended;
			} }

		public ulong MessageId { get {
				return _message.Id;
			} }

		internal PaginatedEmbed(ISocketMessageChannel channel, ulong userid, List<Embed> pages, DiscordSocketClient client)
		{

			_userId = userid;
			_pages = pages;

			_client = client;

			_message = channel.SendMessageAsync(embed: _pages[0]).GetAwaiter().GetResult();

			var emotes = new DEmoji[]
			{
				new DEmoji(FIRST_PAGE),
				new DEmoji(PREV_PAGE),
				new DEmoji(STOP),
				new DEmoji(NEXT_PAGE),
				new DEmoji(LAST_PAGE)
			};

			_message.AddReactionsAsync(emotes).GetAwaiter().GetResult();

			_endgame = new Timer(60 * 1000);
			_endgame.Elapsed += TimerDone;
			_endgame.Start();
		}

		private void TimerDone(object sender, ElapsedEventArgs e)
		{
			_message.RemoveAllReactionsAsync();
			_ended = true;
			_endgame.Stop();
			_endgame.Dispose();
		}

		public async Task ReactionAdded(Cacheable<IUserMessage, ulong> message, ISocketMessageChannel channel, SocketReaction reaction)
		{
			if (message.Id != _message.Id && (_userId != 0 && reaction.UserId != _userId) && !UEmoji.IsEmoji(reaction.Emote.Name))
				return;

			var emojiName = reaction.Emote.Name;

			switch(emojiName)
			{
				case FIRST_PAGE:
					_pageNum = 0;
					await _message.ModifyAsync(mp => mp.Embed = _pages[_pageNum]);
					break;

				case PREV_PAGE:
					if (_pageNum == 0)
						break;
					_pageNum--;
					await _message.ModifyAsync(mp => mp.Embed = _pages[_pageNum]);
					break;

				case STOP:
					await _message.RemoveAllReactionsAsync();
					_ended = true;
					_endgame.Stop();
					_endgame.Dispose();
					break;

				case NEXT_PAGE:
					if (_pageNum == _pages.Count - 1)
						break;
					_pageNum++;
					await _message.ModifyAsync(mp => mp.Embed = _pages[_pageNum]);
					break;

				case LAST_PAGE:
					_pageNum = _pages.Count - 1;
					await _message.ModifyAsync(mp => mp.Embed = _pages[_pageNum]);
					break;
			}

			if (emojiName != STOP)
			{
				await _message.RemoveReactionAsync(reaction.Emote, reaction.User.GetValueOrDefault());
				_endgame.Stop();
				_endgame.Start();
			}
		}
	}
}
