﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Marx.Utility
{
	public class CommandInfoComparer : IEqualityComparer<CommandInfo>
	{
		public bool Equals(CommandInfo x, CommandInfo y)
		{
			if (x == null && y == null)
				return true;

			if (x == null || y == null)
				return false;

			if (x.Name == y.Name && x.Summary == y.Summary)
				return true;

			return false;
		}

		public int GetHashCode(CommandInfo obj)
		{
			return obj.Name.GetHashCode() + obj.Summary.GetHashCode();
		}
	}
}
