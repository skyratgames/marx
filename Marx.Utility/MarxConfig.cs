﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marx.Utility
{
	public class MarxConfig
	{
		public MarxBot Bot { get; set; }
	}


	public class MarxBot
	{
		public string Token { get; set; }

		public string Prefix { get; set; }

		public int DisciplinaryActionTimer { get; set; }

		public string MongoString { get; set; }
		public string DbName { get; set; }

		public string RedisString { get; set; }

		public ulong[] Administrators { get; set; }
	}

}
