﻿using System;

namespace Marx.Utility
{
    public class RegionalIndicator
    {
        private static readonly string[] indicators =
        {
            "\uD83C\uDDE6",
            "\uD83C\uDDE7",
            "\uD83C\uDDE8",
            "\uD83C\uDDE9",
            "\uD83C\uDDEA",
            "\uD83C\uDDEB",
            "\uD83C\uDDEC",
            "\uD83C\uDDED",
            "\uD83C\uDDEE",
            "\uD83C\uDDEF",
            "\uD83C\uDDF0",
            "\uD83C\uDDF1",
            "\uD83C\uDDF2",
            "\uD83C\uDDF3",
            "\uD83C\uDDF4",
            "\uD83C\uDDF5",
            "\uD83C\uDDF6",
            "\uD83C\uDDF7",
            "\uD83C\uDDF8",
            "\uD83C\uDDF9",
            "\uD83C\uDDFA",
            "\uD83C\uDDFB",
            "\uD83C\uDDFC",
            "\uD83C\uDDFD",
            "\uD83C\uDDFE",
            "\uD83C\uDDFF",
        };

        private string Indicator { get; set; }

        public RegionalIndicator(int letter)
        {
            if (letter < 0 || letter > 25)
            {
                throw new InvalidRegionalIndicatorException(letter);
            }

            this.Indicator = indicators[letter];
        }

        public RegionalIndicator(char letter)
        {
            letter = Char.ToLower(letter);
            char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z' };
            int index = Array.IndexOf(chars, letter);

            if (index == -1)
            {
                throw new InvalidRegionalIndicatorException(letter);
            }

            this.Indicator = indicators[index];
        }

        public override string ToString()
        {
            return this.Indicator;
        }
    }
}
