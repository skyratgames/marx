using System;

namespace Marx.Utility
{
    public class InvalidRegionalIndicatorException : Exception
    {
        public InvalidRegionalIndicatorException()
        {

        }

        public InvalidRegionalIndicatorException(char character) : base($"Invalid character for Regional Indicator: {character}.")
        {

        }

        public InvalidRegionalIndicatorException(int index) : base($"Invalid index for Regional Indicator: {index}. Must be from 0-25.")
        {

        }
    }
}
