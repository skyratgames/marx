namespace Marx.Database
{
    public class DatabaseResult
    {
        public bool Success;
        public string Reason;

        public static DatabaseResult FromSuccess()
        {
            return new DatabaseResult(true, "");
        }

        public static DatabaseResult FromError(string reason)
        {
			return new DatabaseResult(false, reason);
        }

		private DatabaseResult(bool succ, string res)
		{
			Success = succ;
			Reason = res;
		}
    }
}
