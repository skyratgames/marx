using Discord;
using Marx.Database.Model;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Marx.Database
{
    public class DbContext
    {
        private static string _connString;
        private static string _dbName;
        private static IMongoClient _client;

        public DbContext(string connString, string dbName)
        {
            DbContext._connString = connString;
            DbContext._dbName = dbName;
            _client = new MongoClient(_connString);
        }

        public IMongoDatabase GetDatabase()
        {
            return _client.GetDatabase(_dbName);
        }

        public DBGuild GetGuild(ulong guildId)
        {
            return this.GetDatabase().GetCollection<DBGuild>("guilds").Find(g => g.Id == guildId).FirstOrDefault();
        }

        public async Task<DBGuild> GetGuildAsync(ulong guildId)
        {
            return await this.GetDatabase().GetCollection<DBGuild>("guilds").Find(g => g.Id == guildId).FirstOrDefaultAsync();
        }
    }
}
