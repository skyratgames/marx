namespace Marx.Database
{
    [System.Serializable]
    public class IEmoteSerializationException : System.Exception
    {
        public IEmoteSerializationException() { }
        public IEmoteSerializationException(string message) : base(message) { }
    }
}
