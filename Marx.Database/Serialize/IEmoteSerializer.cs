using Discord;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using NeoSmart.Unicode;
using System;

namespace Marx.Database
{
    public class IEmoteSerializer : SerializerBase<IEmote>
    {
        public override IEmote Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var type = context.Reader.GetCurrentBsonType();

            if (type == BsonType.String)
            {
                var s = context.Reader.ReadString();

                Emote.TryParse(s, out Emote emote);
                if (emote != null)
                {
                    return emote;
                }
                else
                {
                    if (NeoSmart.Unicode.Emoji.IsEmoji(s))
                    {
                        return new Discord.Emoji(s);
                    }
                    throw new IEmoteSerializationException("Invalid emote.");
                }
            }
            else
            {
                throw new IEmoteSerializationException($"Invalid type {nameof(type)}");
            }
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, IEmote value)
        {
            if (value is Emote emote)
                context.Writer.WriteString(emote.ToString());
            else if (value is Discord.Emoji emoji)
                context.Writer.WriteString(emoji.Name);
        }
    }
}
