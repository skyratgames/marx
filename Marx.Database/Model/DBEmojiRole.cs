using Discord;
using Marx.Cache;
using MongoDB.Bson.Serialization.Attributes;

namespace Marx.Database.Model
{
    public class DBEmojiRole
    {
        [BsonElement("emoji")]
        [BsonSerializer(typeof(IEmoteSerializer))]
        [Cacheable]
        public IEmote Emote { get; set; }

        [BsonElement("role")]
        [Cacheable]
        public ulong Role { get; set; }
    }
}
