using Discord;
using Marx.Cache;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace Marx.Database.Model
{
    public class DBQuickPollSettings
    {
        [BsonElement("affirmative")]
        [BsonSerializer(typeof(IEmoteSerializer))]
        [Cacheable]
        public IEmote AffirmativeEmote { get; set; }

        [BsonElement("negative")]
        [BsonSerializer(typeof(IEmoteSerializer))]
        [Cacheable]
        public IEmote NegativeEmote { get; set; }

        public DBQuickPollSettings()
        {
            this.AffirmativeEmote = new Emoji("👍");
            this.NegativeEmote = new Emoji("👎");
        }

        public void Update(ulong guildId, DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

            var filter = Builders<DBGuild>.Filter.Eq("_id", guildId);
            var update = Builders<DBGuild>.Update.Set("settings.quickPollSettings", this);

            gCol.UpdateOne(filter, update);

            DBGuild gObj = gCol.Find(g => g.Id == guildId).FirstOrDefault();
            cache.Store(guildId.ToString(), gObj);
        }

        public async Task UpdateAsync(ulong guildId, DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

            var filter = Builders<DBGuild>.Filter.Eq("_id", guildId);
            var update = Builders<DBGuild>.Update.Set("settings.quickPollSettings", this);

            await gCol.UpdateOneAsync(filter, update);

            DBGuild gObj = await gCol.Find(g => g.Id == guildId).FirstOrDefaultAsync();
            await cache.StoreAsync(guildId.ToString(), gObj);
        }
    }
}
