using Marx.Cache;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Marx.Database.Model
{
    public class DBTag
    {
        [BsonElement("name")]
        [Cacheable]
        public string Name { get; set; }
        [BsonElement("aliases")]
        [Cacheable]
        public List<string> Aliases { get; set; }
        [BsonElement("content")]
        [Cacheable]
        public string Content { get; set; }

		public DBTag() : this("unnamed", "content"){}

		public DBTag(string name, string content) : this(name, content, new List<string>()) { }

		public DBTag(string name, string content, List<string> aliases)
		{
			Name = name;
			Content = content;
			Aliases = aliases;
		}
    }
}
