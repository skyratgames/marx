using Marx.Cache;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marx.Database.Model
{
    public class DBSettings
    {
        [BsonElement("prefix")]
        [Cacheable]
        public string Prefix { get; set; }

		[BsonElement("mute_role")]
		public ulong MuteRoleId { get; set; }

        [BsonElement("quickPollSettings")]
        [Cacheable]
        public DBQuickPollSettings QuickPollSettings { get; set; }

        public DBSettings()
        {
            this.Prefix = "!";
			this.MuteRoleId = 0;
            this.QuickPollSettings = new DBQuickPollSettings();
        }

        public void Update(ulong guildId, DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

            var filter = Builders<DBGuild>.Filter.Eq("_id", guildId);
            var update = Builders<DBGuild>.Update.Set("settings", this);

            gCol.UpdateOne(filter, update);

            DBGuild gObj = gCol.Find(g => g.Id == guildId).FirstOrDefault();
            cache.Store(guildId.ToString(), gObj);
        }

        public async Task UpdateAsync(ulong guildId, DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

            var filter = Builders<DBGuild>.Filter.Eq("_id", guildId);
            var update = Builders<DBGuild>.Update.Set("settings", this);

            await gCol.UpdateOneAsync(filter, update);

            DBGuild gObj = await gCol.Find(g => g.Id == guildId).FirstOrDefaultAsync();
            await cache.StoreAsync(guildId.ToString(), gObj);
        }
    }
}
