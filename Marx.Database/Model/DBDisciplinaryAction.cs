using Marx.Cache;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marx.Database.Model
{
    public enum DBDisciplinaryActionType
    {
        Mute,
        Kick,
        Ban
    }

    public class DBDisciplinaryAction
    {
        [BsonId]
        public ObjectId Id;
        [BsonElement("type")]
        public DBDisciplinaryActionType Type { get; set; }
        [BsonElement("userId")]
        public ulong UserId { get; set; }
        [BsonElement("reason")]
        public string Reason { get; set; }
        [BsonElement("expires")]
        public DateTime Expires { get; set; }
		[BsonElement("expiration_processed")]
		public bool ExpirationProcessed { get; set; } = false;

		public void Update(ulong guildId, DbContext context)
		{
			var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

			var filter = Builders<DBGuild>.Filter.Where(g => g.Id == guildId && g.DisciplinaryActions.Any(da => da.Id == Id));
			var update = Builders<DBGuild>.Update.Set(g => g.DisciplinaryActions.ElementAt(-1), this);

			gCol.UpdateOne(filter, update);
		}

		public async Task UpdateAsync(ulong guildId, DbContext context)
		{
			var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");

			var filter = Builders<DBGuild>.Filter.Where(g => g.Id == guildId && g.DisciplinaryActions.Any(da => da.Id == Id));
			var update = Builders<DBGuild>.Update.Set(g => g.DisciplinaryActions.ElementAt(-1), this);

			await gCol.UpdateOneAsync(filter, update);
		}

	}
}
