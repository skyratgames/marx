using System.Collections.Generic;
using System.Threading.Tasks;
using Marx.Cache;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading;

namespace Marx.Database.Model
{
    public class DBGuild
    {
        [BsonId]
        public ulong Id { get; set; }
        [BsonElement("settings")]
        [Cacheable]
        public DBSettings Settings { get; set; }

        [BsonElement("disciplinaryActions")]
        public List<DBDisciplinaryAction> DisciplinaryActions { get; set; }

        [BsonElement("emojiRoleMessages")]
        [Cacheable]
        public List<DBEmojiRoleMessage> EmojiRoleMessages { get; set; }

        [BsonElement("tags")]
        [Cacheable]
        public List<DBTag> Tags { get; set; }

        public DBGuild()
        {
            this.Id = 0;
            this.Settings = new DBSettings();
            this.DisciplinaryActions = new List<DBDisciplinaryAction>();
            this.EmojiRoleMessages = new List<DBEmojiRoleMessage>();
            this.Tags = new List<DBTag>();
        }

        public DBGuild(ulong id)
        {
            this.Id = id;
            this.Settings = new DBSettings();
            this.DisciplinaryActions = new List<DBDisciplinaryAction>();
            this.EmojiRoleMessages = new List<DBEmojiRoleMessage>();
        }

        public async Task<DatabaseResult> AddTagAsync(DBTag tag, DbContext context, RedisCacheHandler cache)
        {

            DatabaseResult res = null;

            Thread tr = new Thread(() =>
            {

                string tagName = tag.Name;
                var existingTag = this.Tags.Find(t => t.Name == tagName || t.Aliases.Contains(tagName));

                if (existingTag == null)
                {
                    this.Tags.Add(tag);
                    this.Update(context, cache);
                    res = DatabaseResult.FromSuccess();
                }
                else
                {
                    res = DatabaseResult.FromError("There is already a tag with that name or alias");
                }

            });

            tr.Start();

            while (res == null)
            {
                if (tr.IsAlive)
                    await Task.Delay(10);
                else
                    return res;
            }

            return res;
        }

        public async Task<DatabaseResult> RemoveTagAsync(string name, DbContext context, RedisCacheHandler cache)
        {
            DatabaseResult res = null;

            Thread tr = new Thread(() =>
            {
                var tag = this.Tags.Find(t => t.Name == name || t.Aliases.Contains(name));

                if (tag != null)
                {
                    this.Tags.Remove(tag);
                    this.Update(context, cache);
                    res = DatabaseResult.FromSuccess();
                }
                else
                {
                    res = DatabaseResult.FromError("That tag doesn't exist!");
                }
            });

            tr.Start();

            while (res == null)
            {
                if (tr.IsAlive)
                    await Task.Delay(10);
                else
                    return res;
            }

            return res;
        }

        public async Task<DatabaseResult> EditTagAsync(string name, string content, DbContext context, RedisCacheHandler cache)
        {

            DatabaseResult res = null;

            Thread tr = new Thread(() =>
            {
                int index = this.Tags.FindIndex(t => t.Name == name || t.Aliases.Contains(name));

                if (index < 0)
                {
                    res = DatabaseResult.FromError("Tag with that name or alias does not exist.");
                }

                this.Tags[index].Content = content;
                this.Update(context, cache);
                res = DatabaseResult.FromSuccess();
            });

            tr.Start();

            while (res == null)
            {
                if (tr.IsAlive)
                    await Task.Delay(10);
                else
                    return res;
            }

            return res;
        }

        public async Task<DatabaseResult> AddTagAliasAsync(string name, string alias, DbContext context, RedisCacheHandler cache)
        {

            DatabaseResult res = null;

            Thread tr = new Thread(() =>
            {
                var tag = this.Tags.Find(t => t.Name == name || t.Aliases.Contains(name));
                var existingAlias = this.Tags.Find(t => t.Name == alias || t.Aliases.Contains(alias));

                if (existingAlias == null)
                {
                    tag.Aliases.Add(alias);
                    this.Update(context, cache);
                    res = DatabaseResult.FromSuccess();
                }
                else
                {
                    res = DatabaseResult.FromError("There is already a tag with that name, or a tag alias with that name.");
                }
            });
            tr.Start();

            while (res == null)
            {
                if (tr.IsAlive)
                    await Task.Delay(10);
                else
                    return res;
            }

            return res;
        }

        public async Task<DatabaseResult> RemoveTagAliasAsync(string name, string alias, DbContext context, RedisCacheHandler cache)
        {
            DatabaseResult res = null;

            Thread tr = new Thread(() =>
            {
                var tag = this.Tags.Find(t => t.Name == name || t.Aliases.Contains(name));

                if (tag != null)
                {
                    tag.Aliases.Remove(alias);
                    this.Update(context, cache);
                    res = DatabaseResult.FromSuccess();
                }
                else
                {
                    res = DatabaseResult.FromError("That tag doesn't exist!");
                }
            });
            tr.Start();

            while (res == null)
            {
                if (tr.IsAlive)
                    await Task.Delay(10);
                else
                    return res;
            }

            return res;
        }

        public void AddEmojiRoleMessage(DBEmojiRoleMessage message, DbContext context, RedisCacheHandler cache)
        {
            this.EmojiRoleMessages.Add(message);
            this.Update(context, cache);
        }

        public void RemoveEmojiRoleMessage(ulong messageId, DbContext context, RedisCacheHandler cache)
        {
            DBEmojiRoleMessage message = this.EmojiRoleMessages.Where(m => m.MessageId == messageId).FirstOrDefault();
            this.EmojiRoleMessages.Remove(message);
            this.Update(context, cache);
        }

        public async Task AddEmojiRoleMessageAsync(DBEmojiRoleMessage message, DbContext context, RedisCacheHandler cache)
        {
            this.EmojiRoleMessages.Add(message);
            await this.UpdateAsync(context, cache);
        }

        public async Task RemoveEmojiRoleMessageAsync(ulong messageId, DbContext context, RedisCacheHandler cache)
        {
            DBEmojiRoleMessage message = this.EmojiRoleMessages.Where(m => m.MessageId == messageId).FirstOrDefault();
            this.EmojiRoleMessages.Remove(message);
            await this.UpdateAsync(context, cache);
        }

        public void AddDisciplinaryAction(DBDisciplinaryAction action, DbContext context)
        {
            this.DisciplinaryActions.Add(action);
            this.Update(context);
        }

        public void RemoveDisciplinaryAction(ObjectId actionId, DbContext context)
        {
            DBDisciplinaryAction action = this.DisciplinaryActions.Where(a => a.Id == actionId).FirstOrDefault();
            this.DisciplinaryActions.Remove(action);
            this.Update(context);
        }

        public async Task<DatabaseResult> AddDisciplinaryActionAsync(DBDisciplinaryAction action, DbContext context)
        {
            this.DisciplinaryActions.Add(action);
            await this.UpdateAsync(context);
            return DatabaseResult.FromSuccess();
        }

        public async Task RemoveDisciplinaryActionAsync(ObjectId actionId, DbContext context)
        {
            DBDisciplinaryAction action = this.DisciplinaryActions.Where(a => a.Id == actionId).FirstOrDefault();
            this.DisciplinaryActions.Remove(action);
            await this.UpdateAsync(context);
        }

        private void Update(DbContext context)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");
            ulong gId = this.Id;
            gCol.ReplaceOne(g => g.Id == gId, this);
        }

        private async Task UpdateAsync(DbContext context)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");
            ulong gId = this.Id;
            await gCol.ReplaceOneAsync(g => g.Id == gId, this);
        }

        public void Update(DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");
            ulong gId = this.Id;
            gCol.ReplaceOne(g => g.Id == gId, this);
            cache.Store(gId.ToString(), this);
        }

        public async Task UpdateAsync(DbContext context, RedisCacheHandler cache)
        {
            var gCol = context.GetDatabase().GetCollection<DBGuild>("guilds");
            ulong gId = this.Id;
            await gCol.ReplaceOneAsync(g => g.Id == gId, this);
            await cache.StoreAsync(gId.ToString(), this);
        }
    }
}
