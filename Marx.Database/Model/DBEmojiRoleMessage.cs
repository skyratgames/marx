using Marx.Cache;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Marx.Database.Model
{
    public class DBEmojiRoleMessage
    {
        [BsonElement("message")]
        [Cacheable]
        public ulong MessageId { get; set; }

        [BsonElement("emojis")]
        [Cacheable]
        public List<DBEmojiRole> EmojiRoles { get; set; }
    }
}
