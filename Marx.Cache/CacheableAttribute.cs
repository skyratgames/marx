﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marx.Cache
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Class, AllowMultiple = true)]
	public class CacheableAttribute : Attribute
	{}
}
