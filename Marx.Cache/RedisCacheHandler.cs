﻿using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;

namespace Marx.Cache
{
	public class RedisCacheHandler
	{
		private ConnectionMultiplexer redis;

		public RedisCacheHandler(string connectionString)
		{

			ConfigurationOptions options = ConfigurationOptions.Parse(connectionString);

			options.AllowAdmin = true;
			options.ClientName = "DaddyMarxIsDaddy";

			redis = ConnectionMultiplexer.Connect(options);

			redis.GetEndPoints().ToList().ForEach(ep =>
			{
				redis.GetServer(ep).FlushAllDatabasesAsync();
			});
		}

		public IDatabase GetDB()
		{
			return redis.GetDatabase();
		}

		/// <summary>
		/// Searches for class-level or parameter-level <seealso cref="CacheableAttribute"/>'s
		/// Converts to JSON and stores to redis.
		/// </summary>
		/// <param name="key">Key to store with</param>
		/// <param name="cacheable">Object to convert</param>
		/// <param name="TTL">Time till expirey, if null don't expire.</param>
		public void Store(string key, object cacheable, TimeSpan TTL)
		{
			if (key == null || cacheable == null)
				throw new ArgumentNullException($"{nameof(key)} or {nameof(cacheable)} are null and shouldn't be.");

			var props = GetCachableProperties(cacheable.GetType());

			var json = GetJsonObject(props, cacheable);

			var db = redis.GetDatabase();
			db.StringSet(key, json.ToString());
			if(TTL != null)
				db.KeyExpire(key, TTL);
		}

		/// <summary>
		/// Searches for class-level or parameter-level <seealso cref="CacheableAttribute"/>'s
		/// Converts to JSON and stores to redis.
		/// Key default expire-time to 12 Hours.
		/// </summary>
		/// <param name="key">Key to store in redis</param>
		/// <param name="cacheable">Object to convert and store inside redis</param>
		public void Store(string key, object cacheable)
		{
			Store(key, cacheable, TimeSpan.FromHours(12));
		}

		/// <summary>
		/// The same as <seealso cref="Store(string, object)"/> but it is async.
		/// </summary>
		/// <param name="key">Redis Key to store</param>
		/// <param name="cacheable">Object to store</param>
		/// <param name="TTL">Timespan for cache</param>
		public async Task StoreAsync(string key, object cacheable, TimeSpan TTL) => await Task.Run(() => Store(key, cacheable, TTL));

		/// <summary>
		/// The same as <seealso cref="Store(string, object)"/> but it is async.
		/// </summary>
		/// <param name="key">Redis Key to store</param>
		/// <param name="cacheable">Object to store</param>
		public async Task StoreAsync(string key, object cacheable) => await Task.Run(() => Store(key, cacheable));

		/// <summary>
		/// Get a cached object from redis
		/// </summary>
		/// <typeparam name="T">Type to convert to from cached data</typeparam>
		/// <param name="key">Location of cached data in redis</param>
		/// <returns>Instance of <typeparamref name="T"/> with information from Cache. Null if not found in cache.</returns>
		public T Get<T>(string key) where T : class, new()
		{

			var db = redis.GetDatabase();

			var dbRes = db.StringGet(key);

			if (dbRes.IsNullOrEmpty)
				return null;

			JObject json = JObject.Parse(dbRes);

			return GetObjectFromJson<T>(json);
		}

		/// <summary>
		/// Removes cached data from redis
		/// </summary>
		/// <param name="key">Key to remove</param>
		/// <returns>True if removed, False if it didn't exist or couldn't be removed.</returns>
		public bool Remove(string key)
		{
			var db = redis.GetDatabase();

			return db.KeyDelete(key);
		}

		private JObject GetJsonObject(List<PropertyInfo> props, object source)
		{

			if (source == null)
				throw new ArgumentNullException($"{nameof(source)} Cannot be null.");

			var json = new JObject();

			props.ForEach(info =>
			{
				if (info.PropertyType.IsValueType || info.PropertyType == typeof(String))
				{
					json.Add(info.Name, new JValue(info.GetValue(source)));
					return;
				}

				if(info.PropertyType.IsAssignableFrom(typeof(IEmote)))
				{
					if (info.GetValue(source) is Emote emote)
						json.Add(info.Name, emote.ToString());
					else if (info.GetValue(source) is Discord.Emoji emoji)
						json.Add(info.Name, emoji.Name);
					return;
				}

				var infoProps = GetCachableProperties(info.PropertyType);

				json.Add(info.Name,GetJsonObject(infoProps, info.GetValue(source)));
			});

			return json;
		}

		private List<PropertyInfo> GetCachableProperties(Type type)
		{
			if (Attribute.IsDefined(type, typeof(CacheableAttribute)))
				return type.GetProperties().ToList();

			return type.GetProperties().Where(
				prop => Attribute.IsDefined(prop, typeof(CacheableAttribute)))
					.ToList();
		}

		private T GetObjectFromJson<T>(JToken json) where T : class, new()
		{

			var outObj = new T();

			var props = GetCachableProperties(typeof(T));

			props.ForEach(prop =>
		   {
			   if (prop.PropertyType.IsValueType || prop.PropertyType == typeof(String))
				   prop.SetValue(outObj, Convert.ChangeType(json[prop.Name], prop.PropertyType), null);
			   else if (prop.PropertyType.IsAssignableFrom(typeof(IEmote)))
			   {
				   Emote.TryParse(json[prop.Name].ToString(), out Emote emote);
				   if (emote != null)
					   prop.SetValue(outObj, emote);
				   if (NeoSmart.Unicode.Emoji.IsEmoji(json[prop.Name].ToString()))
				   {
					   var emoji = (IEmote)new Discord.Emoji(json[prop.Name].ToString());
					   prop.SetValue(outObj, emoji, null);
				   }
			   }
 			   else
			   {
				   MethodInfo method = GetType().GetMethod("GetObjectFromJson", BindingFlags.NonPublic | BindingFlags.Instance);
				   MethodInfo generic = method.MakeGenericMethod(new Type[] { prop.PropertyType });

				   var ret = generic.Invoke(this, new object[] { json[prop.Name] });

				   prop.SetValue(outObj, ret, null);
			   }
		   });

			return outObj;
		}

	}
}
